new Vue({
    el: '#app',

    data: {
        ws: null, // Our websocket
        newMsg: '', // Holds new messages to be sent to the server
        chatContent: '', // A running list of chat messages displayed on the screen
        email: null, // Email address used for grabbing an avatar
        joined: false, // True if email has been filled in
        channel: ''
    },

    created: function() {
    },

    methods: {
        send: function () {
            if (this.newMsg != '') {
                this.ws.send(
                    JSON.stringify({
                        email: this.email,
                        message: $('<p>').html(this.newMsg).text()
                    }
                ));
                this.newMsg = ''; // Reset newMsg
            }
        },

        join: function () {
            var self = this;
            
            if (!this.email) {
                Materialize.toast('You must enter an email', 2000);
                return
            }
            this.email = $('<p>').html(this.email).text();
            this.channel = $('<p>').html(this.channel).text();
            this.joined = true;

            var channelName = this.channel != '' ? this.channel : 'all';

            this.ws = new WebSocket('ws://' + window.location.host + '/' + channelName + '/ws');
            this.ws.addEventListener('message', function(e) {
                var msg = JSON.parse(e.data);
                self.chatContent += '<div class="chip">'
                        + '<img src="' + self.gravatarURL(msg.email) + '">' // Avatar
                        + msg.email
                    + '</div>'
                    + emojione.toImage(msg.message) + '<br/>'; // Parse emojis

                var element = document.getElementById('chat-messages');
                element.scrollTop = element.scrollHeight; // Auto scroll to the bottom
            });
        },

        gravatarURL: function(email) {
            return 'http://www.gravatar.com/avatar/' + CryptoJS.MD5(email);
        }
    }
});