package main

type Hub struct {
    id string

    // Registered clients.
    clients map[*Client]bool

    // Inbound messages from the clients.
    broadcast chan Message

    // Register requests from the clients.
    register chan *Client

    // Unregister requests from clients.
    unregister chan *Client
}

func newHub(id string) (hub *Hub) {
    hub = &Hub{
        id: id,
        broadcast:  make(chan Message),
        register:   make(chan *Client),
        unregister: make(chan *Client),
        clients:    make(map[*Client]bool),
    }

    go hub.run()

    return hub
}

func (h *Hub) run() {
    for {
        select {
        case client := <- h.register:
            h.clients[client] = true
        case client := <- h.unregister:
            if _, ok := h.clients[client]; ok {
                delete(h.clients, client)
                close(client.send)
            }
        case message := <- h.broadcast:
            for client := range h.clients {
                select {
                case client.send <- message:
                default:
                    close(client.send)
                    delete(h.clients, client)
                }
            }
        }
    }
}