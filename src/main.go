package main

import (
    "log"
    "net/http"

    "github.com/gorilla/mux"
)

func serveChatRoom(hubMap *HubMap, w http.ResponseWriter, r *http.Request) {
    params := mux.Vars(r)
    channel := params["channel"]

    serveWs(hubMap, channel, w, r)
}

func main() {
    hubMap := newHubMap()
    rtr := mux.NewRouter()
    
    rtr.HandleFunc("/{channel:[a-z]+}/ws", func(w http.ResponseWriter, r *http.Request) {
        serveChatRoom(hubMap, w, r)
    })
    rtr.PathPrefix("/").Handler(http.FileServer(http.Dir("../public")))

    http.Handle("/", rtr)
    err := http.ListenAndServe(":8000", nil)
    if err != nil {
        log.Fatal("ListenAndServe: ", err)
    }
}
