package main

import (
    "sync"
)

// The HubMap maintains a list of all Hubs/Rooms in the chat
type HubMap struct {
    sync.RWMutex
    hubs    map[string]*Hub
}

// Get by key
func (hm *HubMap) Get(key string) *Hub {
    hm.RLock()
    defer hm.RUnlock()

    // Does this hub exist yet
    hub, ok := hm.hubs[key]

    if !ok {
        // Create a new hub
        hub = newHub(key)
        hm.hubs[key] = hub
    }

    return hub
}

func newHubMap() *HubMap {
    return &HubMap{
        hubs: make(map[string]*Hub),
    }
}