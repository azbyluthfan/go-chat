# Go Chat

This is a simple chat web app written in Go

Just run the following

```
cd ./src
go get github.com/gorilla/websocket
go get github.com/gorilla/mux
go run *.go
```

Then point your browser to http://localhost:8000